<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class FP_Controller extends MX_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'profile/m_profile',
      'navigation/m_navigation',
    ));
  }

  function render($content, $data = NULL)
  {
    // Pagination config
    $config_pagination["full_tag_open"] = '<ul class="pagination pagination-sm no-margin pull-right">';
    $config_pagination["full_tag_close"] = '</ul>';

    $config_pagination["first_link"] = "&laquo;";
    $config_pagination["first_tag_open"] = "<li>";
    $config_pagination["first_tag_close"] = "</li>";

    $config_pagination["last_link"] = "&raquo;";
    $config_pagination["last_tag_open"] = "<li>";
    $config_pagination["last_tag_close"] = "</li>";

    $config_pagination['next_link'] = '&gt;';
    $config_pagination['next_tag_open'] = '<li>';
    $config_pagination['next_tag_close'] = '</li>';

    $config_pagination['prev_link'] = '&lt;';
    $config_pagination['prev_tag_open'] = '<li>';
    $config_pagination['prev_tag_close'] = '<li>';
    $config_pagination['cur_tag_open'] = '<li class="active"><a href="#">';
    $config_pagination['cur_tag_close'] = '</a></li>';
    $config_pagination['num_tag_open'] = '<li>';
    $config_pagination['num_tag_close'] = '</li>';
    $config_pagination['num_links'] = 5;
    $this->pagination->initialize($config_pagination);

    // Load profile
    $data['profile'] = $this->m_profile->get_first();
    $data['navigation'] = $this->m_config->get_nav();

    // Templating
    $data['header'] = $this->load->view('front/template/header', $data, TRUE);
    $data['navbar'] = $this->load->view('front/template/navbar', $data, TRUE);
    $data['content'] = $this->load->view($content, $data, TRUE);
    $data['footer'] = $this->load->view('front/template/footer', $data, TRUE);

    $this->load->view('front/template/index', $data);
  }
}
