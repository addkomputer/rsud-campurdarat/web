<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-0">
        <div class="col-sm-6">
          <h5 class="m-0 text-dark"><i class="<?= @$menu['icon'] ?>"></i> <?= @$menu['menu_name'] ?></h5>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Artikel</li>
            <li class="breadcrumb-item active"><?= @$menu['menu_name'] ?></li>
            <li class="breadcrumb-item active"><?= ($id == null) ? 'Tambah' : 'Ubah'; ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Form <?= $menu['menu_name'] ?></h3>
            </div>
            <form id="form" action="<?= site_url() . '/' . $menu['controller'] . '/save/' . $id ?>" method="post" autocomplete="off" enctype="multipart/form-data">
              <div class="card-body">
                <div class="flash-error" data-flasherror="<?= $this->session->flashdata('flash_error') ?>"></div>
                <input type="hidden" class="form-control form-control-sm" name="gallery_id" id="gallery_id" value="<?= @$main['gallery_id'] ?>">
                <?php if ($id != null) : ?>
                  <input type="hidden" class="form-control form-control-sm" name="old" id="old" value="<?= @$main['gallery_id'] ?>" required>
                <?php endif; ?>
                <div class="form-group row">
                  <label for="gallery_title" class="col-sm-2 col-form-label text-right">Judul <span class="text-danger">*</span></label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-sm" name="gallery_title" id="gallery_title" value="<?= @$main['gallery_title'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="category_id" class="col-sm-2 col-form-label text-right">Konten <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <textarea class="editor" id="content" name="content"><?= @$main['content'] ?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="gallery_title" class="col-sm-2 col-form-label text-right">Penulis <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="author" id="author" value="<?= @$main['author'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="menu" class="col-sm-2 col-form-label text-right">Gambar Judul</label>
                  <div class="col-sm-3">
                    <input type="file" name="feature_image" id="feature_image" <?= ($id == null) ? 'requrired' : '' ?>>
                    <input type="hidden" name="old_feature_image" id="old_feature_image" value="<?= @$main['feature_image'] ?>">
                  </div>
                </div>
                <?php if ($id != null) : ?>
                  <div class="form-group row">
                    <label for="menu" class="col-sm-2 col-form-label text-right"></label>
                    <div class="col-sm-2">
                      <img src="<?= base_url() ?>images/gallery/<?= @$main['feature_image'] ?>" alt="<?= @$main['feature_image'] ?>" class="img-thumbnail">
                    </div>
                  </div>
                <?php endif; ?>
                <div class="form-group row">
                  <label for="gallery_title" class="col-sm-2 col-form-label text-right">Diterbitkan pada <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm datetimepicker" name="published_at" id="published_at" value="<?= @reverse_date(@$main['published_at'], '-', 'full_date', ' '); ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="category_id" class="col-sm-2 col-form-label text-right">Status <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <select class="form-control form-control-sm select2" name="status" id="status">
                      <option <?= @$main['status'] == 'draft' ? 'selected' : ''; ?> value="draft">Draf</option>
                      <option <?= @$main['status'] == 'publish' ? 'selected' : ''; ?> value="publish">Terbit</option>
                      <option <?= @$main['status'] == 'archive' ? 'selected' : ''; ?> value="archive">Arsip</option>
                    </select>
                  </div>
                </div>
                <hr>
                <h5 class="mt-4">Gambar Terkait</h5>
                <div id="image_box">
                  <?php foreach ($gallery_image as $r) : ?>
                    <div class="form-group row mb-3">
                      <label for="menu" class="col-sm-2 col-form-label text-right">Gambar Terkait</label>
                      <div class="col-sm-5">
                        <div class="row">
                          <div class="col-6">
                            <img src="<?= base_url() ?>images/gallery/<?= @$r['gallery_image_file'] ?>" alt="<?= @$main['gallery_image_name'] ?>" class="img-thumbnail">
                          </div>
                        </div>
                        <div class="mt-2">
                          <a class="btn btn-xs btn-success" href="<?= base_url() . 'images/gallery/' . $r['gallery_image_file'] ?>" target="_blank"><i class="fas fa-eye"></i> Lihat</a>
                          <a class="btn btn-xs btn-danger btn-delete" href="<?= site_url() . '/' . $menu['controller'] . '/delete_gallery_image/' . @$main['gallery_id'] . '/' . $r['gallery_image_id'] ?>"><i class="fas fa-trash-alt"></i> Hapus</a>
                          <div class="mt-2"><strong>Judul Gambar</strong></div>
                          <input class="form-control form-control-sm" type="text" name="gallery_image_name[]" id="gallery_image_name[]" value="<?= $r['gallery_image_name'] ?>">
                          <input type="hidden" name="old_gallery_image_name[]" id="old_gallery_image_name[]" value="<?= @$r['gallery_image_name'] ? $r['gallery_image_name'] : ' ' ?>">
                          <input type="hidden" name="gallery_image_id[]" id="gallery_image_id[]" value="<?= @$r['gallery_image_id'] ?>">
                        </div>
                      </div>
                    </div>
                  <?php endforeach; ?>
                </div>
                <div class="form-group row mt-4">
                  <div class="col-sm-2 offset-2">
                    <button type="button" class="btn btn-xs btn-default" onclick="add_row_image()"><i class="fas fa-plus"></i> Tambah Gambar</button>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-10 offset-md-2">
                    <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                    <a class="btn btn-sm btn-default btn-cancel" href="<?= site_url() . '/' . $menu['controller'] . '/' . $menu['url'] ?>"><i class="fas fa-times"></i> Batal</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {
        gallery_name: {
          remote: {
            type: 'post',
            url: "<?= site_url() . '/' . $menu['controller'] . '/ajax/check_id/' . $id ?>",
            data: {
              'gallery_name': function() {
                return $('#gallery_name').val();
              }
            },
            dataType: 'json'
          }
        }
      },
      messages: {
        gallery_name: {
          remote: "Kategori sudah digunakan"
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })

  function add_row_image() {
    var html = '<div class="form-group row mb-3 related-image-row">' +
      '<label for="menu" class="col-sm-2 col-form-label text-right">Gambar Terkait</label>' +
      '<div class="col-sm-5">' +
      '<input type="file" name="gallery_image[]" id="gallery_image[]">' +
      '<div class="mt-2"><strong>Judul Gambar</strong></div>' +
      '<div class="row">' +
      '<div class="col-md-11">' +
      '<input class="form-control form-control-sm" type="text" name="gallery_image_name[]" id="gallery_image_name[]">' +
      '</div>' +
      '<div class="col-md-1">' +
      '<button type="button" class="btn btn-sm btn-danger" onclick="delete_row_image(this)"><i class="fas fa-times"></i></button>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>';
    $("#image_box").append(html);
  }

  function delete_row_image(obj) {
    $(obj).closest('.related-image-row').remove();
  }

  function add_row_file() {
    var html = '<div class="form-group row mb-3 related-file-row">' +
      '<label for="menu" class="col-sm-2 col-form-label text-right">Berkas Terkait </label>' +
      '<div class="col-sm-5">' +
      '<input type="file" name="related_file[]" id="related_file[]">' +
      '<div class="mt-2"><strong>Judul Berkas</strong></div>' +
      '<div class="row">' +
      '<div class="col-md-11">' +
      '<input class="form-control form-control-sm" type="text" name="related_file_name[]" id="related_file_name[]">' +
      '</div>' +
      '<div class="col-md-1">' +
      '<button type="button" class="btn btn-sm btn-danger" onclick="delete_row_file(this)"><i class="fas fa-times"></i></button>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>';
    $("#file_box").append(html);
  }

  function delete_row_file(obj) {
    $(obj).closest('.related-file-row').remove();
  }
</script>