<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Article extends MY_Controller
{

  var $menu_id, $menu, $cookie;

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'config/m_config',
      'm_article',
      'category/m_category'
    ));

    $this->menu_id = '22';
    $this->menu = $this->m_config->get_menu($this->menu_id);
    if ($this->menu == null) redirect(site_url() . '/error/error_403');

    //cookie 
    $this->cookie = get_cookie_menu($this->menu_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'published_at', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) 0;
  }

  public function index()
  {
    authorize($this->menu, '_read');
    //cookie
    $this->cookie['cur_page'] = $this->uri->segment(3, 0);
    $this->cookie['total_rows'] = $this->m_article->all_rows($this->cookie);
    set_cookie_menu($this->menu_id, $this->cookie);
    //main data
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_article->list_data($this->cookie);
    $data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    //set pagination
    set_pagination($this->menu, $this->cookie);
    //render
    $this->render('index', $data);
  }

  public function form($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    if ($id == null) {
      create_log(2, $this->menu['menu_name']);
      $data['main'] = null;
    } else {
      create_log(3, $this->menu['menu_name']);
      $data['main'] = $this->m_article->by_field('article_id', $id);
    }
    $data['id'] = $id;
    $data['menu'] = $this->menu;
    $data['category'] = $this->m_category->all_data();
    $data['related_image'] = $this->m_article->related_image_data($id);
    $data['related_file'] = $this->m_article->related_file_data($id);
    $this->render('form', $data);
  }

  public function save($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    html_escape($data = $this->input->post(null, true));
    if (!isset($data['is_active'])) {
      $data['is_active'] = 0;
    }
    $cek = $this->m_article->by_field('article_id', $data['article_id']);
    $data['published_at'] = reverse_date($data['published_at'], '-', 'full_date', ' ');
    if ($id == null) {
      if ($cek != null) {
        $this->session->set_flashdata('flash_error', 'Kode sudah ada di sistem.');
        redirect(site_url() . '/' .  $this->menu['controller']  . '/form/');
      }
      $data['article_id'] = $this->uuid->v4();
      if (!empty($_FILES["feature_image"]["name"])) {
        $data['feature_image'] = $this->_uploadFeatureImage();
      } else {
        $data['feature_image'] = 'no-image.png';
      }
      $this->_uploadRelatedImage($data);
      $this->_uploadRelatedFile($data);
      unset($data['old'], $data['old_feature_image'], $data['related_image'], $data['related_image_name'], $data['related_file'], $data['related_file_name']);
      $this->m_article->save($data, $id);
      create_log(2, $this->menu['menu_name']);
      $this->session->set_flashdata('flash_success', 'Kode berhasil ditambahkan.');
    } else {
      if ($data['old'] != $data['article_id'] && $cek != null) {
        $this->session->set_flashdata('flash_error', 'Kode sudah ada di sistem.');
        redirect(site_url() . '/' . $this->menu['controller'] . '/form/' . $id);
      }
      if (!empty($_FILES["feature_image"]["name"])) {
        $data['feature_image'] = $this->_uploadFeatureImage();
      } else {
        $data['feature_image'] = $data['old_feature_image'];
      }
      foreach ($data['related_image_name'] as $key => $val) {
        if (@$data['old_related_image_name'][$key] && $val != $data['old_related_image_name'][$key]) {
          $this->db->update('related_image', ['related_image_name' => $val], ['related_image_id' => $data['related_image_id'][$key]]);
        }
      }
      foreach ($data['related_file_name'] as $key => $val) {
        if (@$data['old_related_file_name'][$key] && $val != $data['old_related_file_name'][$key]) {
          $this->db->update('related_file', ['related_file_name' => $val], ['related_file_id' => $data['related_file_id'][$key]]);
        }
      }
      $this->_uploadRelatedImage($data);
      $this->_uploadRelatedFile($data);
      unset($data['old'], $data['old_feature_image'], $data['related_image'], $data['related_image_name'], $data['related_file'], $data['related_file_name']);
      unset($data['related_image_name'], $data['old_related_image_name'], $data['related_image_id']);
      unset($data['related_file_name'], $data['old_related_file_name'], $data['related_file_id']);
      $this->m_article->save($data, $id);
      create_log(3, $this->menu['menu_name']);
      $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    }
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function delete($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    $this->m_article->delete($id);
    create_log(4, $this->menu['menu_name']);
    $this->session->set_flashdata('flash_success', 'Data berhasil dihapus.');
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function status($type = null, $id = null)
  {
    authorize($this->menu, '_update');
    if ($type == 'enable') {
      $this->m_article->update($id, array('is_active' => 1));
    } else {
      $this->m_article->update($id, array('is_active' => 0));
    }
    create_log(3, $this->this->menu['menu_name']);
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function multiple($type = null)
  {
    $data = $this->input->post(null, true);
    if (isset($data['checkitem'])) {
      foreach ($data['checkitem'] as $key) {
        switch ($type) {
          case 'delete':
            authorize($this->menu, '_delete');
            $this->m_article->delete($key);
            $flash = 'Data berhasil dihapus.';
            $t = 4;
            break;

          case 'enable':
            authorize($this->menu, '_update');
            $this->m_article->update($key, array('is_active' => 1));
            $flash = 'Data berhasil diaktifkan.';
            $t = 3;
            break;

          case 'disable':
            authorize($this->menu, '_update');
            $this->m_article->update($key, array('is_active' => 0));
            $flash = 'Data berhasil dinonaktifkan.';
            $t = 3;
            break;
        }
      }
    }
    create_log($t, $this->menu['menu_name']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function ajax($type = null, $id = null)
  {
    if ($type == 'check_id') {
      $data = $this->input->post();
      $cek = $this->m_article->by_field('article_id', $data['article_id']);
      if ($id == null) {
        echo ($cek != null) ? 'false' : 'true';
      } else {
        echo ($data['article_id'] != $cek['article_id'] && $cek != null) ? 'false' : 'true';
      }
    }
  }

  private function _uploadFeatureImage()
  {
    $config['upload_path']          = FCPATH . '/images/articles/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp|svg';
    $config['encrypt_name']         = true;
    $config['overwrite']            = true;
    $config['max_size']             = 2048 * 1000000;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('feature_image')) {
      return $this->upload->data("file_name");
    } else {
      var_dump($this->upload->display_errors());
      die();
    }

    return "no-image.png";
  }

  public function _uploadRelatedImage($data)
  {
    $config['upload_path']          = FCPATH . '/images/articles/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp|svg';
    $config['encrypt_name']         = true;
    $config['overwrite']            = true;
    $config['max_size']             = 2048 * 1000000;
    $this->load->library('upload', $config);

    $number_of_image = 0;
    if (!empty($_FILES['related_image']['name'])) {
      $image = $_FILES['related_image'];
      $number_of_image = sizeof($image['name']);
    }

    for ($i = 0; $i < $number_of_image; $i++) {
      if ($image['error'][$i] == 0) {
        $_FILES['image']['name'] = $image['name'][$i];
        $_FILES['image']['type'] = $image['type'][$i];
        $_FILES['image']['tmp_name'] = $image['tmp_name'][$i];
        $_FILES['image']['error'] = $image['error'][$i];
        $_FILES['image']['size'] = $image['size'][$i];
        $last = count(@$data['old_related_image_name']);
        $this->upload->initialize($config);
        // we retrieve the number of files that were uploaded
        if ($this->upload->do_upload('image')) {
          //insert into table
          $d = array(
            'related_image_id' => $this->uuid->v4(),
            'source_id' => $data['article_id'],
            'related_image_name' => $data['related_image_name'][$last + $i],
            'related_image_file' => $this->upload->data("file_name"),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('user_fullname')
          );
          $this->db->insert('related_image', $d);
        } else {
          var_dump($this->upload->display_errors());
          die();
        }
      }
    }
  }

  public function _uploadRelatedFile($data)
  {
    $config['upload_path']          = FCPATH . '/files/articles/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp|svg|xls|xlsx|doc|docx|ppt|pptx|pdf';
    $config['encrypt_name']         = true;
    $config['overwrite']            = true;
    $config['max_size']             = 2048 * 1000000;

    $this->load->library('upload', $config);

    $number_of_file = 0;
    if (!empty($_FILES['related_file']['name'])) {
      $file = $_FILES['related_file'];
      $number_of_file = sizeof($file['name']);
    }

    for ($i = 0; $i < $number_of_file; $i++) {
      if ($file['error'][$i] == 0) {
        $_FILES['file']['name'] = $file['name'][$i];
        $_FILES['file']['type'] = $file['type'][$i];
        $_FILES['file']['tmp_name'] = $file['tmp_name'][$i];
        $_FILES['file']['error'] = $file['error'][$i];
        $_FILES['file']['size'] = $file['size'][$i];
        $last = count(@$data['old_related_file_name']);
        $this->upload->initialize($config);
        // we retrieve the number of files that were uploaded
        if ($this->upload->do_upload('file')) {
          //insert into table
          $d = array(
            'related_file_id' => $this->uuid->v4(),
            'source_id' => $data['article_id'],
            'related_file_name' => $data['related_file_name'][$last + $i],
            'related_file_file' => $this->upload->data("file_name"),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('user_fullname')
          );
          $this->db->insert('related_file', $d);
        } else {
          var_dump($this->upload->display_errors());
          die();
        }
      }
    }
  }

  public function delete_related_image($article_id, $related_image_id)
  {
    $this->m_article->related_image_delete($related_image_id);
    redirect(site_url() . '/' . $this->menu['controller'] . '/form/' . $article_id);
  }

  public function delete_related_file($article_id, $related_file_id)
  {
    $this->m_article->related_file_delete($related_file_id);
    redirect(site_url() . '/' . $this->menu['controller'] . '/form/' . $article_id);
  }
}
