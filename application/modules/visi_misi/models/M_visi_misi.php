<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_visi_misi extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.visi_misi_name LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT * FROM visi_misi a 
      $where
      ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM visi_misi a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM visi_misi a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_first()
  {
    $sql = "SELECT * FROM visi LIMIT 1";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row;
  }

  function get_misi()
  {
    $sql = "SELECT * FROM misi";
    $query = $this->db->query($sql);
    $row = $query->result_array();
    return $row;
  }

  public function save($data)
  {
    $misi = $data['misi_name'];
    unset($data['misi_name']);

    $this->db->where('visi_id', '01')->delete('misi');
    foreach ($misi as $k => $v) {
      $d = array(
        'misi_id' => $k,
        'visi_id' => '01',
        'misi_name' => strtoupper($v)
      );
      $this->db->insert('misi', $d);
    }

    $data['visi_name'] = strtoupper($data['visi_name']);
    $this->db->update('visi', $data);
  }
}
