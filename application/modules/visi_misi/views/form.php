<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-0">
        <div class="col-sm-6">
          <h5 class="m-0 text-dark"><i class="<?= @$menu['icon'] ?>"></i> <?= @$menu['menu_name'] ?></h5>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Berita & Informasi</li>
            <li class="breadcrumb-item active"><?= @$menu['menu_name'] ?></li>
            <li class="breadcrumb-item active">Ubah</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Form <?= $menu['menu_name'] ?></h3>
            </div>
            <form id="form" action="<?= site_url() . '/' . $menu['controller'] . '/save/' ?>" method="post" autocomplete="off">
              <div class="card-body">
                <div class="flash-error" data-flasherror="<?= $this->session->flashdata('flash_error') ?>"></div>
                <h5>Visi</h5>
                <div class="form-group row">
                  <label for="menu" class="col-sm-2 col-form-label text-right">Visi <span class="text-danger">*</span></label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-sm text-uppercase" name="visi_name" id="visi_name" value="<?= @$main['visi_name'] ?>" required>
                  </div>
                </div>
                <h5>Misi</h5>
                <div id="misi_box">

                </div>
                <div class="form-group row mt-2">
                  <label for="menu" class="col-sm-2 col-form-label text-right"></label>
                  <div class="col-sm-4">
                    <button id="tambah" type="button" class="btn btn-default btn-xs" onclick="add_row()"><i class="fas fa-plus"></i> Tambah</button>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-10 offset-md-2">
                    <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                    <a class="btn btn-sm btn-default btn-cancel" href="<?= site_url() . '/' . $menu['controller'] . '/' . $menu['url'] ?>"><i class="fas fa-times"></i> Batal</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });


    <?php foreach ($misi as $k => $v) : ?>
      add_row('<?= $v['misi_name'] ?>');
    <?php endforeach; ?>
  })

  function add_row(value = '') {
    var html = '<div class="form-group row mb-2 misi-row">' +
      '<div class="col-sm-2"></div>' +
      '<div class="col-sm-4">' +
      '<input type="text" class="form-control form-control-sm text-uppercase" name="misi_name[]" value="' + value + '" required>' +
      '</div>' +
      '<div class="col-sm-1">' +
      '<button type="button" class="btn btn-sm btn-danger" onclick="del_row(this)"><i class="fas fa-trash-alt"></i></button>' +
      '</div>' +
      '</div>';
    $("#misi_box").append(html);
  }

  function del_row(o) {
    $(o).closest('.misi-row').remove()
  }
</script>