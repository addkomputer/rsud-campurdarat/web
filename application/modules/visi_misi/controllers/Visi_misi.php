<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Visi_misi extends MY_Controller
{

  var $menu_id, $menu, $cookie;

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'config/m_config',
      'm_visi_misi'
    ));

    $this->menu_id = '27';
    $this->menu = $this->m_config->get_menu($this->menu_id);
    if ($this->menu == null) redirect(site_url() . '/error/error_403');

    //cookie 
    $this->cookie = get_cookie_menu($this->menu_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'role_name', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) 0;
  }

  public function index()
  {
    redirect(site_url() . '/' . $this->menu['controller'] . '/form');
  }

  public function form()
  {
    $data['main'] = $this->m_visi_misi->get_first();
    $data['misi'] = $this->m_visi_misi->get_misi();
    $data['menu'] = $this->menu;
    $this->render('form', $data);
  }

  public function save()
  {
    $data = html_escape($data = $this->input->post(null, true));
    $this->m_visi_misi->save($data);
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }
}
