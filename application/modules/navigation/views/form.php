<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-0">
        <div class="col-sm-6">
          <h5 class="m-0 text-dark"><i class="<?= @$menu['icon'] ?>"></i> <?= @$menu['menu_name'] ?></h5>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Berita & Informasi</li>
            <li class="breadcrumb-item active"><?= @$menu['menu_name'] ?></li>
            <li class="breadcrumb-item active"><?= ($id == null) ? 'Tambah' : 'Ubah'; ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Form <?= $menu['menu_name'] ?></h3>
            </div>
            <form id="form" action="<?= site_url() . '/' . $menu['controller'] . '/save/' . $id ?>" method="post" autocomplete="off">
              <div class="card-body">
                <div class="flash-error" data-flasherror="<?= $this->session->flashdata('flash_error') ?>"></div>
                <div class="form-group row">
                  <label for="type" class="col-sm-2 col-form-label text-right">Tipe</label>
                  <div class="col-sm-2">
                    <select class="form-control form-control-sm select2-hidden" name="type" id="type">
                      <option <?= (@$main['type'] == 1) ? 'selected' : '' ?> value="1">Induk</option>
                      <option <?= (@$main['type'] == 2) ? 'selected' : '' ?> value="2">Menu</option>
                    </select>
                  </div>
                </div>
                <div id="parent-container" class="form-group row">
                  <label for="parent_id" class="col-sm-2 col-form-label text-right">Induk</label>
                  <div class="col-sm-4">
                    <select class="form-control form-control-sm select2" name="parent_id" id="parent_id">
                      <option value="">---</option>
                      <?php foreach ($all as $r) : ?>
                        <option value="<?= $r['navigation_id'] ?>" <?= (@$main['parent_id'] == $r['navigation_id']) ? 'selected' : '' ?>><?= $r['navigation_id'] ?> - <?= $r['navigation_name'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="id" class="col-sm-2 col-form-label text-right">Kode <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm" name="navigation_id" id="navigation_id" value="<?= @$main['navigation_id'] ?>" required>
                    <?php if ($id != null) : ?>
                      <input type="hidden" class="form-control form-control-sm" name="old" id="old" value="<?= @$main['navigation_id'] ?>" required>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="menu" class="col-sm-2 col-form-label text-right">Navigasi <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control form-control-sm" name="navigation_name" id="navigation_name" value="<?= @$main['navigation_name'] ?>" required>
                  </div>
                </div>
                <div id="icon-container" class="form-group row">
                  <label for="icon" class="col-sm-2 col-form-label text-right">Ikon <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm" name="icon" id="icon" value="<?= @$main['icon'] ?>" required>
                  </div>
                </div>
                <div id="source-container" class="form-group row">
                  <label for="type" class="col-sm-2 col-form-label text-right">Sumber <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <select class="form-control form-control-sm select2-hidden" name="source" id="source">
                      <option value="">---</option>
                      <option <?= (@$main['source'] == 1) ? 'selected' : '' ?> value="1">Halaman</option>
                      <option <?= (@$main['source'] == 2) ? 'selected' : '' ?> value="2">Artikel</option>
                      <option <?= (@$main['source'] == 3) ? 'selected' : '' ?> value="3">Galeri</option>
                      <option <?= (@$main['source'] == 4) ? 'selected' : '' ?> value="4">Internal</option>
                      <option <?= (@$main['source'] == 5) ? 'selected' : '' ?> value="5">Eksternal</option>
                    </select>
                  </div>
                </div>
                <div id="article-container" class="form-group row">
                  <label for="article_id" class="col-sm-2 col-form-label text-right">Artikel <span class="text-danger">*</span></label>
                  <div class="col-sm-6">
                    <select class="form-control form-control-sm select2" name="article_id" id="article_id">
                      <option value="">---</option>
                      <?php foreach ($article as $r) : ?>
                        <option value="<?= $r['article_id'] ?>" <?= (@$main['article_id'] == $r['article_id']) ? 'selected' : '' ?>><?= $r['article_title'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div id="page-container" class="form-group row">
                  <label for="page_id" class="col-sm-2 col-form-label text-right">Halaman <span class="text-danger">*</span></label>
                  <div class="col-sm-6">
                    <select class="form-control form-control-sm select2" name="page_id" id="page_id">
                      <option value="">---</option>
                      <?php foreach ($page as $r) : ?>
                        <option value="<?= $r['page_id'] ?>" <?= (@$main['page_id'] == $r['page_id']) ? 'selected' : '' ?>><?= $r['page_title'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div id="gallery-container" class="form-group row">
                  <label for="gallery_id" class="col-sm-2 col-form-label text-right">Galeri <span class="text-danger">*</span></label>
                  <div class="col-sm-6">
                    <select class="form-control form-control-sm select2" name="gallery_id" id="gallery_id">
                      <option value="">---</option>
                      <?php foreach ($gallery as $r) : ?>
                        <option value="<?= $r['gallery_id'] ?>" <?= (@$main['gallery_id'] == $r['gallery_id']) ? 'selected' : '' ?>><?= $r['gallery_title'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div id="internal-container" class="form-group row">
                  <label for="internal" class="col-sm-2 col-form-label text-right">Url Internal<span class="text-danger">*</span></label>
                  <div class="col-sm-6">
                    <div class="input-group input-group-sm">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><?= site_url() . '/' ?></span>
                      </div>
                      <input type="text" class="form-control form-control-sm" name="internal" id="internal" value="<?= @$main['internal'] ?>" required>
                    </div>
                  </div>
                </div>
                <div id="external-container" class="form-group row">
                  <label for="external" class="col-sm-2 col-form-label text-right">Url Eksternal<span class="text-danger">*</span></label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-sm" name="external" id="external" value="<?= @$main['external'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="url" class="col-sm-2 col-form-label text-right">Aktif</label>
                  <div class="col-sm-3">
                    <div class="pretty p-icon">
                      <input class="icheckbox" type="checkbox" name="is_active" id="is_active" value="1" <?php if (@$main) {
                                                                                                            echo (@$main['is_active'] == 1) ? 'checked' : '';
                                                                                                          } else {
                                                                                                            echo 'checked';
                                                                                                          } ?>>
                      <div class="state">
                        <i class="icon fas fa-check"></i><label></label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-10 offset-md-2">
                    <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                    <a class="btn btn-sm btn-default btn-cancel" href="<?= site_url() . '/' . $menu['controller'] . '/' . $menu['url'] ?>"><i class="fas fa-times"></i> Batal</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {
        navigation_id: {
          remote: {
            type: 'post',
            url: "<?= site_url() . '/' . $menu['controller'] . '/ajax/check_id/' . $id ?>",
            data: {
              'navigation_id': function() {
                return $('#navigation_id').val();
              }
            },
            dataType: 'json'
          }
        }
      },
      messages: {
        navigation_id: {
          remote: "Kode sudah digunakan"
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });

    //type onchange
    $('#type').on('select2:select', function(e) {
      get_type($(this).val());
    });

    $('#source').on('select2:select', function(e) {
      get_source($(this).val());
    });

    function get_source(id) {
      console.log(id);
      $("#article-container").hide();
      $("#article_id").val('').trigger('change');

      $("#page-container").hide();
      $("#page_id").val('').trigger('change');

      $("#gallery-container").hide();
      $("#gallery_id").val('').trigger('change');

      $("#internal-container").hide();
      $("#internal_id").val('');

      $("#external-container").hide();
      $("#external_id").val('');
      switch (id) {
        case '1':
          $("#page-container").show();
          break;
        case '2':
          $("#article-container").show();
          break;
        case '3':
          $("#gallery-container").show();
          break;
        case '4':
          $("#internal-container").show();
          break;
        case '5':
          $("#external-container").show();
          break;
      }
    }

    function get_type(val) {
      if (val == 2) {
        $("#access-container").show();
        $("#parent-container").show();
        $("#controller-container").show();
        $("#url-container").show();
        $("#icon-container").show();
        $("#source-container").show();
      } else {
        $("#access-container").hide();

        $("#parent").val('').change();
        $("#parent-container").hide();

        $("#controller").val('#');
        $("#controller-container").hide();

        $("#url").val('#');
        $("#url-container").hide();

        $("#source-container").hide();

        $("#parent_id").val('').trigger('change');

        $("#article-container").hide();
        $("#article_id").val('').trigger('change');

        $("#page-container").hide();
        $("#page_id").val('').trigger('change');

        $("#gallery-container").hide();
        $("#gallery_id").val('').trigger('change');

        $("#internal-container").hide();
        $("#internal_id").val('');

        $("#external-container").hide();
        $("#external_id").val('');
      }
    }

    <?php if ($id == null) : ?>
      get_type(1);
    <?php else : ?>
      get_type(<?= @$main['type'] ?>);
      $("#type").val(<?= @$main['type'] ?>).trigger('change');
      get_source('<?= @$main['source'] ?>');
      $("#source").val('<?= @$main['source'] ?>').trigger('change');
    <?php endif; ?>
  })
</script>