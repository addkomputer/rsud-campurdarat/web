<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_front extends CI_Model
{

  public function slideshow()
  {
    return $this->db->order_by('order_item')->get('slideshow')->result_array();
  }

  public function galleries($limit = null)
  {
    $limit_str = '';
    if ($limit != null) {
      $limit_str = "LIMIT $limit";
    }
    return $this->db->query(
      "SELECT a.* FROM gallery a WHERE a.status = 'publish' ORDER BY published_at DESC $limit_str"
    )->result_array();
  }

  public function get_gallery($id)
  {
    return $this->db->query(
      "SELECT a.* FROM gallery a WHERE a.status = 'publish' AND a.gallery_id = '$id'"
    )->row_array();
  }

  public function get_gallery_image($id)
  {
    return $this->db->query(
      "SELECT a.* FROM gallery_image a WHERE a.gallery_id = '$id'"
    )->result_array();
  }

  public function article($limit = null)
  {
    $limit_str = '';
    if ($limit != null) {
      $limit_str = "LIMIT $limit";
    }
    return $this->db->query(
      "SELECT a.*, b.category_name FROM article a JOIN category b ON a.category_id = b.category_id WHERE a.status = 'publish' ORDER BY published_at DESC $limit_str"
    )->result_array();
  }

  public function article_category($category_id)
  {
    $limit_str = '';
    return $this->db->query(
      "SELECT 
        a.*, b.category_name 
      FROM article a 
      JOIN category b ON a.category_id = b.category_id 
      WHERE a.status = 'publish' AND a.category_id = '$category_id'
      ORDER BY published_at DESC $limit_str"
    )->result_array();
  }

  public function get_category($category_id)
  {
    return $this->db->where('category_id', $category_id)->get('category')->row_array();
  }

  public function get_article($id)
  {
    $result = $this->db->query(
      "SELECT a.*, b.category_name FROM article a JOIN category b ON a.category_id = b.category_id WHERE a.status = 'publish' AND a.article_id = '$id'"
    )->row_array();
    if ($result != null) {
      $result['image'] = $this->get_related_image($result['article_id']);
      $result['file'] = $this->get_related_file($result['article_id']);
      return $result;
    } else {
      return null;
    }
  }

  public function get_related_image($source_id)
  {
    return $this->db->query(
      "SELECT * FROM related_image WHERE source_id = '$source_id'"
    )->result_array();
  }

  public function get_related_file($source_id)
  {
    return $this->db->query(
      "SELECT * FROM related_file WHERE source_id = '$source_id'"
    )->result_array();
  }

  public function article_popular()
  {
    return $this->db->query(
      "SELECT a.*, b.category_name FROM article a 
      JOIN category b ON a.category_id = b.category_id 
      ORDER BY a.read_count DESC LIMIT 3"
    )->result_array();
  }

  public function category()
  {
    return $this->db->query(
      "SELECT a.* FROM category a"
    )->result_array();
  }

  public function gallery($limit = null)
  {
    $limit_str = '';
    if ($limit != null) {
      $limit_str = "LIMIT $limit";
    }
    return $this->db->query(
      "SELECT a.* FROM gallery a where a.status = 'publish' $limit_str"
    )->result_array();
  }

  public function all_gallery($limit = null)
  {
    $limit_str = '';
    if ($limit != null) {
      $limit_str = "LIMIT $limit";
    }
    return $this->db->query(
      "SELECT a.* FROM gallery_image a  ORDER BY RAND() $limit_str"
    )->result_array();
  }

  public function get_page($id)
  {
    return $this->db->where('page_id', $id)->get('page')->row_array();
  }

  public function article_detail($id)
  {
    $data = $this->db->where('article_id', $id)->get('article')->row_array();
    if ($data != null) {
      $data['image'] = $this->db->where('article_id', $id)->get('related_image')->result_array();
      $data['file'] = $this->db->where('article_id', $id)->get('related_file')->result_array();
    }
    return $data;
  }

  public function penduduk_kecamatan($tahun, $tabel_id = 1)
  {
    $prefix = substr($tahun, 2, 2);
    $table = $prefix . '_dat_kategori';
    if ($this->db->table_exists($table)) {

      return $this->db->query(
        "SELECT a.kategori_nm, IFNULL(b.nilai,0) as nilai FROM " . $prefix . "_dat_kategori a
      LEFT JOIN (
        SELECT 
          a.kategori_id, SUM(a.nilai) as nilai
        FROM " . $prefix . "_dat_statistik a
        JOIN " . $prefix . "_dat_tabel_kolom b ON a.kolom_id = b.kolom_id
        WHERE 
          a.tabel_id = 1 AND is_nilai = 'INPUT'
        GROUP BY a.kategori_id 
      ) b ON b.kategori_id = a.kategori_id
      WHERE a.tabel_id = '$tabel_id'"
      )->result_array();
    } else {
      return null;
    }
  }

  public function penduduk_umur($tahun, $tabel_id = 2)
  {
    $prefix = substr($tahun, 2, 2);
    $table = $prefix . '_dat_kategori';
    // echo $table;
    // die;
    if ($this->db->table_exists($table)) {
      return $this->db->query(
        "SELECT a.kategori_nm, IFNULL(b.nilai,0) as nilai FROM " . $prefix . "_dat_kategori a
      LEFT JOIN (
        SELECT 
          a.kategori_id, SUM(a.nilai) as nilai
        FROM " . $prefix . "_dat_statistik a
        JOIN " . $prefix . "_dat_tabel_kolom b ON a.kolom_id = b.kolom_id
        WHERE 
          a.tabel_id = '$tabel_id' AND is_nilai = 'INPUT'
        GROUP BY a.kategori_id 
      ) b ON b.kategori_id = a.kategori_id
      WHERE a.tabel_id = '$tabel_id'"
      )->result_array();
    } else {
      return null;
    }
  }

  public function list_tabel($tahun = null)
  {
    if ($tahun == null) {
      return null;
    };
    $prefix = substr($tahun, 2, 2);
    $table = $prefix . '_dat_tabel';
    if ($this->db->table_exists($table)) {
      return $this->db->query(
        "SELECT * FROM $table"
      )->result_array();
    } else {
      return null;
    }
  }

  public function statistik_detail($tahun, $tabel_id)
  {
    $prefix = substr($tahun, 2, 2);
    $data = array();
    $data['tabel'] = $this->db->query("SELECT * FROM " . $prefix . "_dat_tabel WHERE tabel_id = '$tabel_id'")->row_array();
    $data['kolom'] = $this->get_tabel_kolom($tahun, $tabel_id);
    $data['tabel_kolom'] = $this->db->query("SELECT * FROM " . $prefix . "_dat_kolom WHERE tabel_id = '$tabel_id'")->result_array();
    $data['kategori'] = $this->db->query("SELECT * FROM " . $prefix . "_dat_kategori WHERE tabel_id = '$tabel_id'")->result_array();
    foreach ($data['kategori'] as $k => $v) {
      foreach ($data['kolom'] as $k2 => $v2) {
        $row = $this->db
          ->where('tabel_id', $tabel_id)
          ->where('kategori_id', $v['kategori_id'])
          ->where('kolom_id', $v2['kolom_id'])
          ->get($prefix . '_dat_statistik')->row_array();
        if ($row == null) {
          $nilai = 0;
        } else {
          $nilai = $row['nilai'];
        }
        $data['kategori'][$k][$v2['kolom_id']] = $nilai;
      };
    }
    var_dump($data);
    die;
    return $data;
  }

  public function get_tabel_kolom($tahun, $tabel_id, $parent_id = null)
  {
    $prefix = substr($tahun, 2, 2);
    $sql_where = '';
    $sql_where .= ($parent_id != '') ? "a.parent_id = '$parent_id'" : 'a.parent_id IS NULL || a.parent_id = ""';

    $query = $this->db->query(
      "SELECT a.kolom_id, judul_kolom 
      FROM
        " . $prefix . "_dat_tabel_kolom a 
      WHERE
        $sql_where
      ORDER BY a.kolom_id"
    );
    if ($query->num_rows() > 0) {
      $result = $query->result_array();
      foreach ($result as $key => $val) {
        $result[$key]['child'] = $this->get_tabel_kolom($tahun, $tabel_id, $result[$key]['kolom_id']);
      }
      return $result;
    } else {
      return array();
    }
  }

  public function statistik_grafik($tahun = 2018, $tabel_id = 1)
  {
    $prefix = substr($tahun, 2, 2);
    return $this->db->query(
      "SELECT a.kategori_nm, IFNULL(b.nilai,0) as nilai FROM 18_dat_kategori a
      LEFT JOIN (
        SELECT 
          a.kategori_id, SUM(a.nilai) as nilai
        FROM " . $prefix . "_dat_statistik a
        JOIN " . $prefix . "_dat_tabel_kolom b ON a.kolom_id = b.kolom_id
        WHERE 
          a.tabel_id = 1 AND is_nilai = 'INPUT'
        GROUP BY a.kategori_id 
      ) b ON b.kategori_id = a.kategori_id
      WHERE a.tabel_id = '$tabel_id'"
    )->result_array();
  }

  public function link()
  {
    return $this->db->get('link_terkait')->result_array();
  }

  public function list_gallery()
  {
    return $this->db->get_where('gallery', ['status' => 'publish'])->result_array();
  }

  public function list_video()
  {
    return $this->db->get_where('gallery_video', ['status' => 'publish'])->result_array();
  }

  public function get_video($id)
  {
    return $this->db->where('gallery_id', $id)->get('gallery_video')->row_array();
  }

  public function gallery_image($id)
  {
    return $this->db->where('gallery_id', $id)->get('gallery_image')->result_array();
  }

  public function gambar_terkait($id)
  {
    return $this->db->where('article_id', $id)->get('related_image')->result_array();
  }

  public function file_terkait($id)
  {
    return $this->db->where('article_id', $id)->get('related_file')->result_array();
  }

  public function contact_us_save($data)
  {
    $this->db->insert('contact_us', $data);
  }

  public function tracer_save($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = 'Public';
    $this->db->insert('alumni', $data);
  }

  public function captcha($captcha, $ip)
  {
    return $this->db->where('word', $captcha)->where('ip_address', $ip)->get('captcha')->row_array();
  }

  public function alumni($data)
  {
    return $this->db->where('nisn', $data['nisn'])->where('nama_lengkap', $data['nama_lengkap'])->get('alumni')->row_array();
  }

  public function visi()
  {
    return $this->db->get('visi')->row_array();
  }

  public function misi()
  {
    return $this->db->get('misi')->result_array();
  }
}
