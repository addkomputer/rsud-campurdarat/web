<!-- Footer Start -->
<div class="container-fluid bg-dark footer mt-5 py-5 wow fadeIn" data-wow-delay="0.1s">
  <div class="container py-5">
    <div class="row g-5">
      <div class="col-lg-3 col-md-6">
        <h5 class="text-white mb-4">Lokasi</h5>
        <p class="mb-2"><i class="fa fa-map-marker-alt me-3"></i><?= $profile['address'] ?></p>
        <p class="mb-2"><i class="fa fa-phone-alt me-3"></i><?= $profile['phone'] ?></p>
        <p class="mb-2"><i class="fa fa-envelope me-3"></i><?= $profile['email'] ?></p>
      </div>
      <div class="col-lg-3 col-md-6">
        <h5 class="text-white mb-4">Jam Pelayanan Poliklinik</h5>
        <p class="mb-1">Senin - Sabtu</p>
        <h6 class="text-light">07:00 - selesai</h6>
        <h5 class="text-white mt-4 mb-4">Jam Pelayanan IGD</h5>
        <p class="mb-1">Senin - Minggu</p>
        <h6 class="text-light">24 Jam</h6>
      </div>
    </div>
  </div>
</div>
<!-- Footer End -->


<!-- Copyright Start -->
<div class="container-fluid copyright bg-dark py-4">
  <div class="container text-center">
    <p class="mb-2">Copyright &copy; <a class="fw-semi-bold" href="#"><?= $profile['company_name'] ?></a>, All Right Reserved.</p>
    <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
    <p class="mb-0">Designed By <a class="fw-semi-bold" href="https://htmlcodex.com">HTML Codex</a></p>
  </div>
</div>
<!-- Copyright End -->


<!-- Back to Top -->
<a href="#" class="btn btn-lg btn-primary btn-lg-square rounded-circle back-to-top"><i class="bi bi-arrow-up"></i></a>


<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>front/lib/wow/wow.min.js"></script>
<script src="<?= base_url() ?>front/lib/easing/easing.min.js"></script>
<script src="<?= base_url() ?>front/lib/waypoints/waypoints.min.js"></script>
<script src="<?= base_url() ?>front/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="<?= base_url() ?>front/lib/counterup/counterup.min.js"></script>

<!-- Template Javascript -->
<script src="<?= base_url() ?>front/js/main.js"></script>
</body>

</html>