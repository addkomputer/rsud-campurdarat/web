<!-- Topbar Start -->
<!-- <div class="container-fluid bg-dark px-0">
  <div class="row g-0 d-none d-lg-flex">
    <div class="col-lg-6 ps-5 text-start">
      <div class="h-100 d-inline-flex align-items-center text-white">
        <span>Follow Us:</span>
        <a class="btn btn-link text-light" href=""><i class="fab fa-facebook-f"></i></a>
        <a class="btn btn-link text-light" href=""><i class="fab fa-twitter"></i></a>
        <a class="btn btn-link text-light" href=""><i class="fab fa-linkedin-in"></i></a>
        <a class="btn btn-link text-light" href=""><i class="fab fa-instagram"></i></a>
      </div>
    </div>
    <div class="col-lg-6 text-end">
      <div class="h-100 topbar-right d-inline-flex align-items-center text-white py-2 px-5">
        <span class="fs-5 fw-bold me-2"><i class="fa fa-phone-alt me-2"></i>Call Us:</span>
        <span class="fs-5 fw-bold">+012 345 6789</span>
      </div>
    </div>
  </div>
</div> -->
<!-- Topbar End -->
<!-- Navbar Start -->
<nav class="navbar navbar-expand-lg bg-white navbar-light sticky-top py-0 pe-5">
  <a href="index.html" class="navbar-brand ps-5 me-0">
    <img src="<?= base_url() ?>images/logos/<?= $profile['logo1'] ?>" alt="" width="100">
  </a>
  <button type="button" class="navbar-toggler me-0" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <div class="navbar-nav ms-auto p-4 p-lg-0">
      <?php foreach ($navigation as $row) : ?>
        <?php if (count($row['child']) == 0) : ?>
          <a href="<?php
                    switch ($row['source']) {
                      case 1:
                        echo site_url() . '/front/page/' . $row['page_id'];
                        break;

                      case 2:
                        echo site_url() . '/front/article/' . $row['article_id'];
                        break;

                      case 3:
                        echo site_url() . '/front/gallery/' . $row['gallery_id'];
                        break;

                      case 4:
                        echo site_url() . '/' . $row['internal'];
                        break;

                      case 5:
                        echo $row['external'];
                        break;

                      default:
                        echo site_url();
                        break;
                    }
                    ?>" <?= ($row['source'] == 5) ? 'target="_blank"' : '' ?> class="nav-item nav-link"><?= $row['navigation_name'] ?></a>
        <?php else : ?>
          <div class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><?= $row['navigation_name'] ?></a>
            <div class="dropdown-menu bg-light m-0">
              <?php foreach ($row['child'] as $child) : ?>
                <a href="<?php
                          switch ($child['source']) {
                            case 1:
                              echo site_url() . '/front/page/' . $child['page_id'];
                              break;

                            case 2:
                              echo site_url() . '/front/article/' . $child['article_id'];
                              break;

                            case 3:
                              echo site_url() . '/front/gallery/' . $child['gallery_id'];
                              break;

                            case 4:
                              echo site_url() . '/' . $child['internal'];
                              break;

                            case 5:
                              echo $child['external'];
                              break;

                            default:
                              echo site_url();
                              break;
                          }
                          ?>" <?= ($child['source'] == 5) ? 'target="_blank"' : '' ?> class="dropdown-item"><?= $child['navigation_name'] ?></a>
              <?php endforeach; ?>
            </div>
          </div>
        <?php endif; ?>
      <?php endforeach; ?>
    </div>
    <a href="<?= base_url() ?>../skm/index.php" target="_blank" class="btn btn-primary px-3 d-none d-lg-block" style="margin-right:10px">Survei Kepuasan</a>
    <a href="<?= site_url() ?>/auth/login" class="btn btn-outline-primary px-3 d-none d-lg-block">Login</a>
  </div>
</nav>
<!-- Navbar End -->