<!-- Page Header Start -->
<div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s">
  <div class="container py-5">
    <h1 class="display-3 text-white animated slideInRight">Galeri</h1>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb animated slideInRight mb-0">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Galeri</a></li>
      </ol>
    </nav>
  </div>
</div>
<!-- Page Header End -->
<section id="blog-section" class="padding_bottom padding_top">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-9 col-xs-12">
        <?php foreach ($galleries as $row) : ?>
          <div class="blog-box">
            <div id="web" class="blog-box-img">
              <img src="<?= base_url() ?>images/gallery/<?= $row['feature_image'] ?>" alt="image">
              <?php
              $date = DateTime::createFromFormat('Y-m-d H:i:s', $row['published_at']);
              ?>
              <div class="date-tag">
                <span class="date-sec">
                  <p><?= $date->format('d') ?></p>
                  <p><?= $date->format('M') ?></p>
                </span>
              </div>
            </div>
            <div class="blog-box-detail">
              <h2><a href="<?= site_url() . '/front/gallery/' . $row['gallery_id'] ?>"><?= $row['gallery_title'] ?></a></h2>
              <div class="blog-tags">
                <a href="javascript:void(0)"><span>By</span> <?= $row['author'] ?>,</a>
                <a href="javascript:void(0)"><span><i class="fa fa-eye" aria-hidden="true"></i></span> <?= $row['read_count'] ?></a>
              </div>
              <?= word_limiter($row['content'], 50) ?>
              <br>
              <a href="javascript:void(0)" class="blog-button">Selengkapnya</a>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <?php $this->load->view('side') ?>
      </div>
    </div>
  </div>
</section>