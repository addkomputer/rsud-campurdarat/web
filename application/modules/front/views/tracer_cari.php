<section id="blog-section" class="padding_bottom padding_top">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="blog-box">
          <div class="blog-box-detail">
            <h2><a href="javascript:void(0)">Cari Data Alumni</a></h2>
            <br>
            <form id="form" method="POST" action="<?= site_url() . '/front/tracer_cari_action' ?>" autocomplete="off">
              <div class="card-body">
                <input type="hidden" class="form-control form-control-sm" name="alumni_id" id="alumni_id" value="" required>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Nama Lengkap <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm text-uppercase" name="nama_lengkap" id="nama_lengkap" value="<?= @$main['nama_lengkap'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">NISN <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="number" class="form-control form-control-sm" name="nisn" id="nisn" value="" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right"> <span class="text-danger"></span></label>
                  <div class="col-sm-2">
                    <?php echo $captcha['image']; ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Kode <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="number" class="form-control form-control-sm" name="captcha" id="captcha" value="" required>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-4">
                    <button type="submit" class="btn btn-md btn-default btn-submit"><i class="fas fa-search"></i> Cari</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <?php $this->load->view('side') ?>
      </div>
    </div>
  </div>
</section>
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {
        captcha: {
          remote: {
            type: 'post',
            url: "<?= site_url() . '/front/ajax/captcha' ?>",
            data: {
              'captcha': function() {
                return $('#captcha').val();
              }
            },
            dataType: 'json'
          }
        }
      },
      messages: {
        captcha: {
          remote: "Kode salah!"
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })
</script>