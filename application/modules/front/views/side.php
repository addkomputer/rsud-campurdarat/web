<div class="blog-sidebar-box">
  <h3 style="margin-bottom:20px !important">Kategori</h3>
  <ul class="list-group">
    <?php foreach ($category as $row) : ?>
      <li class="list-group-item"><a href="<?= site_url() ?>/front/category/<?= $row['category_id'] ?>"><?= $row['category_name'] ?></a></li>
    <?php endforeach; ?>
  </ul>
  <ul class="blog-sidebar-category">
  </ul>
</div>
<div class="blog-sidebar-box mt-5">
  <h3>Artikel Terpopuler</h3>
  <br>
  <?php foreach ($article_popular as $row) : ?>
    <div class="row">
      <div class="col-4">
        <img class="img-thumbnail" src="<?= base_url() ?>images/articles/<?= $row['feature_image'] ?>" alt="img">
      </div>
      <div class="col-8">
        <?php $date = DateTime::createFromFormat('Y-m-d H:i:s', $row['published_at']); ?>
        <h6><?= $row['article_title'] ?></h6>
        <span class="pull-right"><?= $date->format('d M Y') ?></span>
      </div>
    </div>
  <?php endforeach; ?>
</div>