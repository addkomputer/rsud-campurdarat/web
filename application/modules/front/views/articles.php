<!-- Page Header Start -->
<div class="container-fluid page-header py-5 mb-5 wow fadeIn" data-wow-delay="0.1s">
  <div class="container py-5">
    <h1 class="display-3 text-white animated slideInRight">Berita & Informasi</h1>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb animated slideInRight mb-0">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Berita & Informasi</a></li>
      </ol>
    </nav>
  </div>
</div>
<!-- Page Header End -->
<div class="container-xxl py-5">
  <div class="container">
    <div class="row">
      <div class="col-9">
        <?php foreach ($article as $key => $row) : ?>
          <div class="row">
            <div class="col-3">
              <img class="img-thumbnail" src="<?= base_url() ?>images/articles/<?= $row['feature_image'] ?>" alt="image">
            </div>
            <div class="col-9">
              <h3><a href="<?= site_url() . '/front/article/' . $row['article_id'] ?>"><?= $row['article_title'] ?></a></h3>
              <div class="blog-tags">
                <a href="javascript:void(0)" style="margin-right:10px;"><span><i class="fa fa-user" aria-hidden="true"></i></span> <?= $row['author'] ?></a>
                <a href="javascript:void(0)" style="margin-right:10px;"><span><i class="fa fa-list" aria-hidden="true"></i></span> <?= $row['category_name'] ?></a>
                <a href="javascript:void(0)" style="margin-right:10px;"><span><i class="fa fa-eye" aria-hidden="true"></i></span> <?= $row['read_count'] ?></a>
              </div>
              <?= word_limiter($row['content'], 20) ?>
              <br>
              <a href="javascript:void(0)" class="blog-button mt-3">Selengkapnya...</a>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
      <div class="col-3">
        <?php $this->load->view('side') ?>
      </div>
    </div>
  </div>
</div>