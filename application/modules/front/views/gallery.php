<section id="blog-section" class="padding_bottom padding_top">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="blog-box">
          <div id="web" class="blog-box-img">
            <img src="<?= base_url() ?>images/gallery/<?= $gallery['feature_image'] ?>" alt="image">
            <?php
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $gallery['published_at']);
            ?>
            <div class="date-tag">
              <span class="date-sec">
                <p><?= $date->format('d') ?></p>
                <p><?= $date->format('M') ?></p>
              </span>
            </div>
          </div>
          <div class="blog-box-detail">
            <h2><a href="javascript:void(0)"><?= $gallery['gallery_title'] ?></a></h2>
            <div class="blog-tags">
              <a href="javascript:void(0)"><span>By</span> <?= $gallery['author'] ?>,</a>
              <a href="javascript:void(0)"><span><i class="fa fa-eye" aria-hidden="true"></i></span> <?= $gallery['read_count'] ?></a>
              <a href="javascript:void(0)"><span><i class="fa fa-calendar" aria-hidden="true"></i></span> <?= $date->format('d M Y H:i:s') ?></a>
            </div>
            <?= $gallery['content'] ?>
            Penulis : <?= $gallery['author'] ?><br>
          </div>
        </div>
        <div class="blog-box">
          <div class="blog-box-detail">
            <h2><a href="javascript:void(0)">Gambar Terkait</a></h2>
            <br>
            <div class="row">
              <?php foreach ($gallery_image as $image) : ?>
                <div class="col-md-4" style="margin-bottom:10px">
                  <a href="<?= base_url() ?>images/gallery/<?= $image['gallery_image_file'] ?>" target="_blank">
                    <img class="img-responsive img-thumbnail" src="<?= base_url() ?>images/gallery/<?= $image['gallery_image_file'] ?>" alt="image">
                  </a>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <?php $this->load->view('side') ?>
      </div>
    </div>
  </div>
</section>