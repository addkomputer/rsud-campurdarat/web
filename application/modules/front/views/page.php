<section id="blog-section" class="padding_bottom padding_top">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="blog-box">
          <div id="web" class="blog-box-img">
            <img src="<?= base_url() ?>images/pages/<?= $page['feature_image'] ?>" alt="image">
            <?php
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $page['published_at']);
            ?>
            <div class="date-tag">
              <span class="date-sec">
                <p><?= $date->format('d') ?></p>
                <p><?= $date->format('M') ?></p>
              </span>
            </div>
          </div>
          <div class="blog-box-detail">
            <h2><a href="javascript:void(0)"><?= $page['page_title'] ?></a></h2>
            <div class="blog-tags">
              <a href="javascript:void(0)"><span>By</span> <?= $page['author'] ?>,</a>
              <!-- <a href="javascript:void(0)"><span><i class="fa fa-list" aria-hidden="true"></i></span> <?= $page['category_name'] ?></a> -->
              <a href="javascript:void(0)"><span><i class="fa fa-eye" aria-hidden="true"></i></span> <?= $page['read_count'] ?></a>
              <a href="javascript:void(0)"><span><i class="fa fa-calendar" aria-hidden="true"></i></span> <?= $date->format('d M Y H:i:s') ?></a>
            </div>
            <?= $page['content'] ?>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <?php $this->load->view('side') ?>
      </div>
    </div>
  </div>
</section>