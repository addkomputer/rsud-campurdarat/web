<section id="blog-section" class="padding_bottom padding_top">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="blog-box">
          <div class="blog-box-detail">
            <h2><a href="javascript:void(0)">Tracer Alumni</a></h2>
            <br>
            <form id="form" method="POST" action="<?= site_url() . '/front/tracer_save' ?>" autocomplete="off">
              <div class="card-body">
                <input type="hidden" class="form-control form-control-sm" name="alumni_id" id="alumni_id" value="<?= @$main['alumni_id'] ?>" required>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Nama Lengkap <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm text-uppercase" name="nama_lengkap" id="nama_lengkap" value="<?= @$main['nama_lengkap'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Tahun Lulus <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <select class="form-control form-control-sm" name="tahun_lulus" id="tahun_lulus">
                      <?php for ($i = 2000; $i < date('Y'); $i++) : ?>
                        <option value="<?= $i ?>" <?= (@$main['tahun_lulus'] == $i) ? 'selected' : '' ?>><?= $i ?></option>
                      <?php endfor; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Nomor Induk Siswa</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="nis" id="nis" value="<?= @$main['nis'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">NISN <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="nisn" id="nisn" value="<?= @$main['nisn'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Nomor Ijazah <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="nomor_ijazah" id="nomor_ijazah" value="<?= @$main['nomor_ijazah'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Jenis Kelamin <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <select class="form-control form-control-sm select2" name="jenis_kelamin" id="jenis_kelamin">
                      <option value="L" <?= @($main['jenis_kelamin'] == 'L') ? 'selected' : '' ?>>Laki-laki</option>
                      <option value="P" <?= @($main['jenis_kelamin'] == 'P') ? 'selected' : '' ?>>Perempuan</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">TTL <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="tmp_lahir" id="tmp_lahir" value="<?= @$main['tmp_lahir'] ?>" required>
                  </div>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm datepicker" name="tgl_lahir" id="tgl_lahir" value="<?= @reverse_date($main['tgl_lahir']) ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">NIK</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="nik" id="nik" value="<?= @$main['nik'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Email <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control form-control-sm" name="email" id="email" value="<?= @$main['email'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Telepon <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="telepon" id="telepon" value="<?= @$main['telepon'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Alamat</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" name="alamat" id="alamat" value="<?= @$main['alamat'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Melanjutkan Ke <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <select class="form-control form-control-sm select2" name="lanjut_id" id="lanjut_id">
                      <option value="01">Menganggur</option>
                      <option value="02">Lanjut Studi</option>
                      <option value="03">Bekerja Pemerintahan</option>
                      <option value="04">Bekerja Swasta</option>
                      <option value="05">Wirausaha</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-4">
                    <button type="submit" class="btn btn-md btn-success btn-submit"><i class="fas fa-save"></i> Simpan</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <?php $this->load->view('side') ?>
      </div>
    </div>
  </div>
</section>
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })
</script>