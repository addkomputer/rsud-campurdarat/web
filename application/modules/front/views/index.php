<!-- Page Header Start -->
<div class="container-fluid page-header mb-5 wow fadeIn" data-wow-delay="0.1s" style="height: 600px !important;">
  <div class="container" style="padding-top:200px">
    <h1 class="display-3 text-white animated slideInRight">RSUD Campurdarat</h1>
    <h1 class="display-3 text-white animated slideInRight">Kabupaten Tulungagung</h1>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb animated slideInRight mb-0">
        <li class="breadcrumb-item"><a href="#">Siap</a></li>
        <li class="breadcrumb-item"><a href="#">Inovatif</a></li>
        <li class="breadcrumb-item"><a href="#">Aman</a></li>
        <li class="breadcrumb-item"><a href="#">Profesional</a></li>
      </ol>
    </nav>
  </div>
</div>
<!-- Page Header End -->

<!-- Features Start -->
<div class="container-xxl py-5">
  <div class="container">
    <div class="row g-5 align-items-center">
      <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
        <div class="position-relative me-lg-4">
          <img class="img-fluid w-100" src="<?= base_url() ?>konsultasi.jpeg" alt="">
          <span class="position-absolute top-50 start-100 translate-middle bg-white rounded-circle d-none d-lg-block" style="width: 120px; height: 120px;"></span>
          <button type="button" class="btn-play" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/imUgF0Miy18" data-bs-target="#videoModal">
            <span></span>
          </button>
        </div>
      </div>
      <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
        <p class="fw-medium text-uppercase text-primary mb-2">Mengapa kami?</p>
        <h1 class="display-5 mb-4">Pelayanan kesehatan profesional untuk masyarakat.</h1>
        <p class="mb-4">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit. Aliqu diam amet diam et eos. Clita erat ipsum et lorem et sit, sed stet lorem sit clita duo justo magna dolore erat amet</p>
      </div>
    </div>
  </div>
</div>
<!-- Features End -->


<!-- Facts Start -->
<div class="container-fluid facts my-5 p-5">
  <div class="row g-5">
    <div class="col-md-6 col-xl-3 wow fadeIn" data-wow-delay="0.1s">
      <div class="text-center border p-5">
        <i class="fa fa-home fa-3x text-white mb-3"></i>
        <h1 class="display-2 text-primary mb-0" data-toggle="counter-up">11</h1>
        <span class="fs-5 fw-semi-bold text-white">Poli Rawat Jalan</span>
      </div>
    </div>
    <div class="col-md-6 col-xl-3 wow fadeIn" data-wow-delay="0.3s">
      <div class="text-center border p-5">
        <i class="fa fa-procedures fa-3x text-white mb-3"></i>
        <h1 class="display-2 text-primary mb-0" data-toggle="counter-up">7</h1>
        <span class="fs-5 fw-semi-bold text-white">Bangsal Rawat Inap</span>
      </div>
    </div>
    <div class="col-md-6 col-xl-3 wow fadeIn" data-wow-delay="0.5s">
      <div class="text-center border p-5">
        <i class="fa fa-stethoscope fa-3x text-white mb-3"></i>
        <h1 class="display-2 text-primary mb-0" data-toggle="counter-up">17</h1>
        <span class="fs-5 fw-semi-bold text-white">Dokter</span>
      </div>
    </div>
    <div class="col-md-6 col-xl-3 wow fadeIn" data-wow-delay="0.7s">
      <div class="text-center border p-5">
        <i class="fa fa-users fa-3x text-white mb-3"></i>
        <h1 class="display-2 text-primary mb-0" data-toggle="counter-up">168</h1>
        <span class="fs-5 fw-semi-bold text-white">Pegawai</span>
      </div>
    </div>
  </div>
</div>
<!-- Facts End -->


<div class="container-xxl py-5">
  <div class="container">
    <div class="text-center mx-auto pb-4 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
      <p class="fw-medium text-uppercase text-primary mb-2">Our Services</p>
      <h1 class="display-5 mb-4">We Provide Best Healthcare Services</h1>
    </div>
  </div>
</div>

<!-- Video Modal Start -->
<div class="modal modal-video fade" id="videoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content rounded-0">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLabel">Profile RSUD Campurdarat</h6>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <!-- 16:9 aspect ratio -->
        <div class="ratio ratio-16x9">
          <iframe class="embed-responsive-item" src="" id="video" allowfullscreen allowscriptaccess="always" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Video Modal End -->


<!-- Project Start -->
<div class="container-fluid bg-dark pt-5 my-5 px-0">
  <div class="text-center mx-auto mt-5 wow fadeIn" data-wow-delay="0.1s" style="max-width: 600px;">
    <p class="fw-medium text-uppercase text-primary mb-2">Fasilitas Layanan</p>
    <h1 class="display-5 text-white mb-5">Pelayanan Pasien</h1>
  </div>
  <div class="owl-carousel project-carousel wow fadeIn" data-wow-delay="0.1s">
    <a class="project-item" href="">
      <img class="img-fluid" src="<?= base_url() ?>rajal.jpeg" alt="" style="height:350px !important">
      <div class="project-title">
        <h5 class="text-primary mb-0">Perawatan Pasien</h5>
      </div>
    </a>
    <a class="project-item" href="">
      <img class="img-fluid" src="<?= base_url() ?>igd.jpeg" alt="" style="height:350px !important">
      <div class="project-title">
        <h5 class="text-primary mb-0">Gawat Darurat</h5>
      </div>
    </a>
    <a class="project-item" href="">
      <img class="img-fluid" src="<?= base_url() ?>laborat.jpeg" alt="" style="height:350px !important">
      <div class="project-title">
        <h5 class="text-primary mb-0">Laboratorium</h5>
      </div>
    </a>
    <a class="project-item" href="">
      <img class="img-fluid" src="<?= base_url() ?>radiologi.jpeg" alt="" style="height:350px !important">
      <div class="project-title">
        <h5 class="text-primary mb-0">Radiologi</h5>
      </div>
    </a>
    <a class="project-item" href="">
      <img class="img-fluid" src="<?= base_url() ?>ibs.jpeg" alt="" style="height:350px !important">
      <div class="project-title">
        <h5 class="text-primary mb-0">Bedah</h5>
      </div>
    </a>
  </div>
</div>
<!-- Project End -->


<!-- Team Start -->
<div class="container-xxl py-5">
  <div class="container">
    <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
      <p class="fw-medium text-uppercase text-primary mb-2">Our Team</p>
      <h1 class="display-5 mb-5">Direksi</h1>
    </div>
    <div class="row g-4">
      <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
        <div class="team-item">
          <img class="img-fluid" src="<?= base_url() ?>front/img/rio.png" alt="" style="width:100%;height:300px;">
          <div class="d-flex">
            <div class="flex-shrink-0 btn-square bg-primary" style="width: 90px; height: 90px;">
              <i class="fa fa-2x fa-share text-white"></i>
            </div>
            <div class="position-relative overflow-hidden bg-light d-flex flex-column justify-content-center w-100 ps-4" style="height: 90px;">
              <h6>dr. Rio Ardona, MMRS</h6>
              <small class="text-primary">DIREKTUR RUMAH SAKIT</small>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
        <div class="team-item">
          <img class="img-fluid" src="<?= base_url() ?>no-photo.png" alt="" style="width:100%;height:300px;">
          <div class="d-flex">
            <div class="flex-shrink-0 btn-square bg-primary" style="width: 90px; height: 90px;">
              <i class="fa fa-2x fa-share text-white"></i>
            </div>
            <div class="position-relative overflow-hidden bg-light d-flex flex-column justify-content-center w-100 ps-4" style="height: 90px;">
              <h6>Bangun Setyono, S.Kep</h6>
              <small class="text-primary">KEPALA SUB BAGIAN TATA USAHA</small>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
        <div class="team-item">
          <img class="img-fluid" src="<?= base_url() ?>front/img/dina.png" alt="" style="width:100%;height:300px;">
          <div class="d-flex">
            <div class="flex-shrink-0 btn-square bg-primary" style="width: 90px; height: 90px;">
              <i class="fa fa-2x fa-share text-white"></i>
            </div>
            <div class="position-relative overflow-hidden bg-light d-flex flex-column justify-content-center w-100 ps-4" style="height: 90px;">
              <h6>dr. Dina Nofitria Rahayu</h6>
              <small class="text-primary">KEPALA SEKSI PELAYANAN MEDIK DAN KEPERAWATAN</small>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
        <div class="team-item">
          <img class="img-fluid" src="<?= base_url() ?>no-photo.png" alt="" style="width:100%;height:300px;">
          <div class="d-flex">
            <div class="flex-shrink-0 btn-square bg-primary" style="width: 90px; height: 90px;">
              <i class="fa fa-2x fa-share text-white"></i>
            </div>
            <div class="position-relative overflow-hidden bg-light d-flex flex-column justify-content-center w-100 ps-4" style="height: 90px;">
              <h6>drg. Arif Dwi Cahyono</h6>
              <small class="text-primary">KEPALA SEKSI PELAYANAN PENUNJANG MEDIK DAN NON MEDIK</small>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Team End -->