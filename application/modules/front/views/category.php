<section id="inner-banner">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="inner-banner-detail">
          <p><a href="javascript:void(0)">Berita & Informasi Terbaru</a></p>
          <h2>Kategori</h2>
          <br>
          <h4 style="color:white"><?= $category_current['category_name'] ?></h4>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="blog-section" class="padding_bottom padding_top">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-9 col-xs-12">
        <?php if ($article == null) : ?>
          <div class="blog-box">
            <div class="blog-box-detail">
              <i>Artikel tidak ditemukan. </i>
            </div>
          </div>
        <?php endif; ?>
        <?php foreach ($article as $row) : ?>
          <div class="blog-box">
            <div id="web" class="blog-box-img">
              <img src="<?= base_url() ?>images/articles/<?= $row['feature_image'] ?>" alt="image">
              <?php
              $date = DateTime::createFromFormat('Y-m-d H:i:s', $row['published_at']);
              ?>
              <div class="date-tag">
                <span class="date-sec">
                  <p><?= $date->format('d') ?></p>
                  <p><?= $date->format('M') ?></p>
                </span>
              </div>
            </div>
            <div class="blog-box-detail">
              <h2><a href="<?= site_url() . '/front/article/' . $row['article_id'] ?>"><?= $row['article_title'] ?></a></h2>
              <div class="blog-tags">
                <a href="javascript:void(0)"><span>By</span> <?= $row['author'] ?>,</a>
                <a href="javascript:void(0)"><span><i class="fa fa-list" aria-hidden="true"></i></span> <?= $row['category_name'] ?></a>
                <a href="javascript:void(0)"><span><i class="fa fa-eye" aria-hidden="true"></i></span> <?= $row['read_count'] ?></a>
              </div>
              <?= word_limiter($row['content'], 50) ?>
              <br>
              <a href="javascript:void(0)" class="blog-button">Selengkapnya</a>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <?php $this->load->view('side') ?>
      </div>
    </div>
  </div>
</section>