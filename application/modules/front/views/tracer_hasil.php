<section id="blog-section" class="padding_bottom padding_top">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="blog-box">
          <div class="blog-box-detail">
            <h2><a href="javascript:void(0)">Data Alumni</a></h2>
            <br>
            <?php if (@$main == null) : ?>
              <div class="card-body">
                <h2 class="text-center text-success">Data tidak ditemukan</h2>
                <br>
                <i class="fas fa-band fa-5x"></i>
                <br><br>
                <p>Data alumni tidak ditemukan</p>
                <br>
                <a class="btn btn-lg btn-default" href="<?= site_url() . '/front/tracer_cari' ?>">Cari Lagi</a>
              </div>
            <?php else : ?>
              <div class="card-body">
                <input type="hidden" class="form-control form-control-sm" name="alumni_id" id="alumni_id" value="<?= @$main['alumni_id'] ?>" readonly>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Nama Lengkap</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm text-uppercase" name="nama_lengkap" id="nama_lengkap" value="<?= @$main['nama_lengkap'] ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Tahun Lulus</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm text-uppercase" name="nama_lengkap" id="tahun_lulus" value="<?= @$main['tahun_lulus'] ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Nomor Induk Siswa</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="nis" id="nis" value="<?= @$main['nis'] ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">NISN</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="nisn" id="nisn" value="<?= @$main['nisn'] ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Nomor Ijazah</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="nomor_ijazah" id="nomor_ijazah" value="<?= @$main['nomor_ijazah'] ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Jenis Kelamin</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm" name="jenis_kelamin" id="jenis_kelamin" value="<?= @($main['jenis_kelamin'] == 'L') ? 'Laki-laki' : 'Perempuan' ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">TTL</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="tmp_lahir" id="tmp_lahir" value="<?= @$main['tmp_lahir'] ?>" readonly>
                  </div>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="tgl_lahir" id="tgl_lahir" value="<?= @reverse_date($main['tgl_lahir']) ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">NIK</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="nik" id="nik" value="<?= @$main['nik'] ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Email</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control form-control-sm" name="email" id="email" value="<?= @$main['email'] ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Telepon</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="telepon" id="telepon" value="<?= @$main['telepon'] ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Alamat</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" name="alamat" id="alamat" value="<?= @$main['alamat'] ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label text-right">Melanjutkan Ke</label>
                  <div class="col-sm-3">
                    <select class="form-control form-control-sm" name="lanjut_id" id="lanjut_id" disabled>
                      <option value="01">Menganggur</option>
                      <option value="02">Lanjut Studi</option>
                      <option value="03">Bekerja Pemerintahan</option>
                      <option value="04">Bekerja Swasta</option>
                      <option value="05">Wirausaha</option>
                    </select>
                  </div>
                </div>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <?php $this->load->view('side') ?>
      </div>
    </div>
  </div>
</section>
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })
</script>