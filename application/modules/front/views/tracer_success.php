<section id="blog-section" class="padding_bottom padding_top">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="blog-box">
          <div class="blog-box-detail">
            <h2><a href="javascript:void(0)">Tracer Alumni</a></h2>
            <div class="text-center">
              <h2 class="text-center text-success">Berhasil</h2>
              <br>
              <i class="fas fa-check-circle fa-5x text-success"></i>
              <br><br>
              <p>Tracer alumni berhasil terkirim.</p>
              <br>
              <a class="btn btn-lg btn-success" href="<?= site_url() ?>">Oke</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <?php $this->load->view('side') ?>
      </div>
    </div>
  </div>
</section>
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })
</script>