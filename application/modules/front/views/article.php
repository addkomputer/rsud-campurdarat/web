<section id="blog-section" class="padding_bottom padding_top">
  <div class="container">
    <div class="row mt-5">
      <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="blog-box">
          <div id="web" class="blog-box-img">
            <img style="width: 100%;height:450px;" src="<?= base_url() ?>images/articles/<?= $article['feature_image'] ?>" class="img-thumbnail" alt="image">
            <?php
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $article['published_at']);
            ?>
          </div>
          <br><br>
          <div class="blog-box-detail">
            <h2><a href="javascript:void(0)"><?= $article['article_title'] ?></a></h2>
            <div class="blog-tags">
              <a href="javascript:void(0)" style="margin-right: 10px;"><span>By</span> <?= $article['author'] ?>,</a>
              <a href="javascript:void(0)" style="margin-right: 10px;"><span><i class="fa fa-list" aria-hidden="true"></i></span> <?= $article['category_name'] ?></a>
              <a href="javascript:void(0)" style="margin-right: 10px;"><span><i class="fa fa-eye" aria-hidden="true"></i></span> <?= $article['read_count'] ?></a>
              <a href="javascript:void(0)" style="margin-right: 10px;"><span><i class="fa fa-calendar" aria-hidden="true"></i></span> <?= $date->format('d M Y H:i:s') ?></a>
            </div>
            <?= $article['content'] ?>
            Penulis : <?= $article['author'] ?><br>
            Editor : <?= $article['editor'] ?><br>
            <?php if ($article['origin'] == 'Eksternal') : ?>
              <br>
              Sumber: <a href="<?= $article['origin_url'] ?>" target="_blank"><?= $article['origin_url'] ?></a>
            <?php endif; ?>
          </div>
          <?php if (count(@$article['image']) > 0) : ?>
            <div class="blog-box-detail">
              <h4>Gambar Terkait</h4>
              <br>
              <?php foreach ($article['image'] as $image) : ?>
                <a href="<?= base_url() ?>images/articles/<?= @$image['related_image_file'] ?>" target="_blank"><img src="<?= base_url() ?>images/articles/<?= @$image['related_image_file'] ?>" alt="image" width="200" height="150"></a>
              <?php endforeach; ?>
            </div>
          <?php endif; ?>
          <?php if (count(@$article['file']) > 0) : ?>
            <div class="blog-box-detail">
              <h4>Berkas Terkait</h4>
              <br>
              <table class="table table-striped table-condensed table-bordered">
                <thead>
                  <tr>
                    <th class="text-center" width="50">No.</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center" width="70">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1;
                  foreach ($article['file'] as $file) : ?>
                    <tr>
                      <td class="text-center"><?= $i++ ?></td>
                      <td class="text-left"><?= $file['related_file_name'] ?></td>
                      <td class="text-center">
                        <a class="btn btn-xs btn-success" target="_blank" href="<?= base_url() ?>files/articles/<?= @$file['related_file_file'] ?>"><i class="fa fa-download"></i></a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          <?php endif; ?>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <?php $this->load->view('side') ?>
      </div>
    </div>
  </div>
</section>