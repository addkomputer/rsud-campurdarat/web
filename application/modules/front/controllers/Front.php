<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Front extends FP_Controller
{

  var $menu_id, $menu, $cookie;

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'config/m_config',
      'm_front',
      'slideshow/m_slideshow',
      'navigation/m_navigation'
    ));
  }

  function index()
  {
    $data['navbar'] = 'navbar-transparent';
    $data['article'] = $this->m_front->article(5);
    $data['gallery'] = $this->m_front->gallery(5);
    $data['all_gallery'] = $this->m_front->all_gallery(10);
    $data['slideshow'] = $this->m_front->slideshow();
    $data['category'] = $this->m_front->category();

    $data['cookie'] = $this->cookie;
    $this->render('index', $data);
  }

  function articles()
  {
    $data['category'] = $this->m_front->category();
    $data['article'] = $this->m_front->article();
    $data['article_popular'] = $this->m_front->article_popular();
    $data['gallery'] = $this->m_front->gallery(5);
    $data['all_gallery'] = $this->m_front->all_gallery(10);
    $data['slideshow'] = $this->m_front->slideshow();
    $data['category'] = $this->m_front->category();

    $data['cookie'] = $this->cookie;
    $this->render('articles', $data);
  }

  function galleries()
  {
    $data['category'] = $this->m_front->category();
    $data['galleries'] = $this->m_front->galleries();
    $data['article_popular'] = $this->m_front->article_popular();
    $data['gallery'] = $this->m_front->gallery(5);
    $data['all_gallery'] = $this->m_front->all_gallery(10);
    $data['slideshow'] = $this->m_front->slideshow();
    $data['category'] = $this->m_front->category();

    $data['cookie'] = $this->cookie;
    $this->render('galleries', $data);
  }

  function category($id = null)
  {
    if ($id == null) {
      redirect(site_url());
    } else {
      $data['category'] = $this->m_front->category();
      $data['article'] = $this->m_front->article_category($id);
      $data['article_popular'] = $this->m_front->article_popular();
      $data['category_current'] = $this->m_front->get_category($id);
      $data['gallery'] = $this->m_front->gallery(5);
      $data['all_gallery'] = $this->m_front->all_gallery(10);
      $data['slideshow'] = $this->m_front->slideshow();

      $data['cookie'] = $this->cookie;
      $this->render('category', $data);
    }
  }

  function gallery($id = null)
  {
    $data['gallery'] = $this->m_front->get_gallery($id);
    $data['gallery_image'] = $this->m_front->get_gallery_image($id);
    if ($id == null || $id == '' || $data['gallery'] == null) {
      redirect(site_url());
    }
    $data['category'] = $this->m_front->category();
    $data['article_popular'] = $this->m_front->article_popular();

    $data['cookie'] = $this->cookie;
    $this->render('gallery', $data);
  }

  function article($id = null)
  {
    $data['article'] = $this->m_front->get_article($id);
    if ($id == null || $id == '' || $data['article'] == null) {
      redirect(site_url());
    }
    $data['category'] = $this->m_front->category();
    $data['article_popular'] = $this->m_front->article_popular();
    $data['gallery'] = $this->m_front->gallery(5);
    $data['all_gallery'] = $this->m_front->all_gallery(10);
    $data['slideshow'] = $this->m_front->slideshow();
    $data['category'] = $this->m_front->category();

    $data['cookie'] = $this->cookie;
    $this->render('article', $data);
  }

  function page($id = null)
  {
    $data['page'] = $this->m_front->get_page($id);
    if ($id == null || $id == '' || $data['page'] == null) {
      redirect(site_url());
    }
    $data['category'] = $this->m_front->category();
    $data['article_popular'] = $this->m_front->article_popular();
    $data['gallery'] = $this->m_front->gallery(5);
    $data['all_gallery'] = $this->m_front->all_gallery(10);
    $data['slideshow'] = $this->m_front->slideshow();
    $data['category'] = $this->m_front->category();

    $data['cookie'] = $this->cookie;
    $this->render('page', $data);
  }

  function tracer()
  {
    $data['category'] = $this->m_front->category();
    $data['article_popular'] = $this->m_front->article_popular();
    $data['gallery'] = $this->m_front->gallery(5);
    $data['all_gallery'] = $this->m_front->all_gallery(10);
    $data['slideshow'] = $this->m_front->slideshow();
    $data['category'] = $this->m_front->category();

    $data['cookie'] = $this->cookie;
    $this->render('tracer', $data);
  }

  function tracer_save()
  {
    $data = html_escape($data = $this->input->post(null, true));
    $data['tgl_lahir'] = reverse_date($data['tgl_lahir']);
    $data['nama_lengkap'] = strtoupper($data['nama_lengkap']);
    $this->m_front->tracer_save($data);

    redirect(site_url() . '/front/tracer_success');
  }

  function tracer_success()
  {
    $data['category'] = $this->m_front->category();
    $data['article_popular'] = $this->m_front->article_popular();
    $data['gallery'] = $this->m_front->gallery(5);
    $data['all_gallery'] = $this->m_front->all_gallery(10);
    $data['slideshow'] = $this->m_front->slideshow();
    $data['category'] = $this->m_front->category();

    $data['cookie'] = $this->cookie;
    $this->render('tracer_success', $data);
  }

  function tracer_cari()
  {
    $data['category'] = $this->m_front->category();
    $data['article_popular'] = $this->m_front->article_popular();
    $data['gallery'] = $this->m_front->gallery(5);
    $data['all_gallery'] = $this->m_front->all_gallery(10);
    $data['slideshow'] = $this->m_front->slideshow();
    $data['category'] = $this->m_front->category();

    $vals = array(
      // 'word'          => 'Random word',
      'img_path'      => FCPATH . '/images/captcha/',
      'img_url'       => base_url() . 'images/captcha/',
      'font_path'     => './path/to/fonts/texb.ttf',
      'img_width'     => '100',
      'img_height'    => 30,
      'expiration'    => 7200,
      'word_length'   => 4,
      'font_size'     => 24,
      'img_id'        => 'Imageid',
      'pool'          => '0123456789',

      'colors'        => array(
        'background' => array(255, 255, 255),
        'border' => array(255, 255, 255),
        'text' => array(0, 0, 0),
        'grid' => array(255, 40, 40)
      )
    );

    $cap = create_captcha($vals);
    $d = array(
      'captcha_time'  => $cap['time'],
      'ip_address'    => $this->input->ip_address(),
      'word'          => $cap['word']
    );

    $query = $this->db->insert_string('captcha', $d);
    $this->db->query($query);

    $data['captcha'] = $cap;

    $data['cookie'] = $this->cookie;
    $this->render('tracer_cari', $data);
  }

  function tracer_cari_action()
  {
    $req = html_escape($this->input->post());

    $data['category'] = $this->m_front->category();
    $data['article_popular'] = $this->m_front->article_popular();
    $data['gallery'] = $this->m_front->gallery(5);
    $data['all_gallery'] = $this->m_front->all_gallery(10);
    $data['slideshow'] = $this->m_front->slideshow();
    $data['category'] = $this->m_front->category();

    if ($req == null) {
      redirect(site_url());
    } else {
      $data['main'] = $this->m_front->alumni($req);

      $this->render('tracer_hasil', $data);
    }
  }

  public function ajax($type = null, $id = null)
  {
    if ($type == 'captcha') {
      $captcha = $this->input->post('captcha');
      $ip = $this->input->ip_address();

      $res = $this->m_front->captcha($captcha, $ip);
      if ($res == null) {
        echo 'false';
      } else {
        echo 'true';
      }
    }
  }
}
