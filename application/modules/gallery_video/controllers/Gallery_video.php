<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gallery_video extends MY_Controller
{

  var $menu_id, $menu, $cookie;

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'config/m_config',
      'm_gallery_video',
      'category/m_category'
    ));

    $this->menu_id = '83.1';
    $this->menu = $this->m_config->get_menu($this->menu_id);
    if ($this->menu == null) redirect(site_url() . '/error/error_403');

    //cookie 
    $this->cookie = get_cookie_menu($this->menu_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'gallery_id', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) 0;
  }

  public function index()
  {
    authorize($this->menu, '_read');
    //cookie
    $this->cookie['cur_page'] = $this->uri->segment(3, 0);
    $this->cookie['total_rows'] = $this->m_gallery_video->all_rows($this->cookie);
    set_cookie_menu($this->menu_id, $this->cookie);
    //main data
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_gallery_video->list_data($this->cookie);
    $data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    //set pagination
    set_pagination($this->menu, $this->cookie);
    //render
    $this->render('index', $data);
  }

  public function form($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    if ($id == null) {
      create_log(2, $this->menu['menu_name']);
      $data['main'] = null;
    } else {
      create_log(3, $this->menu['menu_name']);
      $data['main'] = $this->m_gallery_video->by_field('gallery_id', $id);
    }
    $data['id'] = $id;
    $data['menu'] = $this->menu;
    $data['category'] = $this->m_category->all_data();
    $data['gallery_image'] = $this->m_gallery_video->gallery_image_data($id);
    $this->render('form', $data);
  }

  public function save($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    html_escape($data = $this->input->post(null, true));
    if (!isset($data['is_active'])) {
      $data['is_active'] = 0;
    }
    $cek = $this->m_gallery_video->by_field('gallery_id', $data['gallery_id']);
    $data['published_at'] = reverse_date($data['published_at'], '-', 'full_date', ' ');
    if ($id == null) {
      if ($cek != null) {
        $this->session->set_flashdata('flash_error', 'Kode sudah ada di sistem.');
        redirect(site_url() . '/' .  $this->menu['controller']  . '/form/');
      }
      $data['gallery_id'] = $this->uuid->v4();
      $video = explode('/', $data['video']);
      if (substr($video[3], 0, 8) == 'watch?v=') {
        $data['video'] = str_replace('watch?v=', '', $video[3]);
      } else {
        $data['video'] = $video[3];
      };
      unset($data['old']);
      $this->m_gallery_video->save($data, $id);
      create_log(2, $this->menu['menu_name']);
      $this->session->set_flashdata('flash_success', 'Kode berhasil ditambahkan.');
    } else {
      if ($data['old'] != $data['gallery_id'] && $cek != null) {
        $this->session->set_flashdata('flash_error', 'Kode sudah ada di sistem.');
        redirect(site_url() . '/' . $this->menu['controller'] . '/form/' . $id);
      }
      $video = explode('/', $data['video']);
      if (substr($video[3], 0, 8) == 'watch?v=') {
        $data['video'] = str_replace('watch?v=', '', $video[3]);
      } else {
        $data['video'] = $video[3];
      };
      unset($data['old']);
      $this->m_gallery_video->save($data, $id);
      create_log(3, $this->menu['menu_name']);
      $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    }
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function delete($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    $this->m_gallery_video->delete($id);
    create_log(4, $this->menu['menu_name']);
    $this->session->set_flashdata('flash_success', 'Data berhasil dihapus.');
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function status($type = null, $id = null)
  {
    authorize($this->menu, '_update');
    if ($type == 'enable') {
      $this->m_gallery_video->update($id, array('is_active' => 1));
    } else {
      $this->m_gallery_video->update($id, array('is_active' => 0));
    }
    create_log(3, $this->this->menu['menu_name']);
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function multiple($type = null)
  {
    $data = $this->input->post(null, true);
    if (isset($data['checkitem'])) {
      foreach ($data['checkitem'] as $key) {
        switch ($type) {
          case 'delete':
            authorize($this->menu, '_delete');
            $this->m_gallery_video->delete($key);
            $flash = 'Data berhasil dihapus.';
            $t = 4;
            break;

          case 'enable':
            authorize($this->menu, '_update');
            $this->m_gallery_video->update($key, array('is_active' => 1));
            $flash = 'Data berhasil diaktifkan.';
            $t = 3;
            break;

          case 'disable':
            authorize($this->menu, '_update');
            $this->m_gallery_video->update($key, array('is_active' => 0));
            $flash = 'Data berhasil dinonaktifkan.';
            $t = 3;
            break;
        }
      }
    }
    create_log($t, $this->menu['menu_name']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function ajax($type = null, $id = null)
  {
    if ($type == 'check_id') {
      $data = $this->input->post();
      $cek = $this->m_gallery_video->by_field('gallery_id', $data['gallery_id']);
      if ($id == null) {
        echo ($cek != null) ? 'false' : 'true';
      } else {
        echo ($data['gallery_id'] != $cek['gallery_id'] && $cek != null) ? 'false' : 'true';
      }
    }
  }

  private function _uploadFeatureImage()
  {
    $config['upload_path']          = FCPATH . '/images/gallery/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp|svg';
    $config['encrypt_name']         = true;
    $config['overwrite']            = true;
    $config['max_size']             = 2048 * 1000000;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('feature_image')) {
      return $this->upload->data("file_name");
    } else {
      var_dump($this->upload->display_errors());
      die();
    }

    return "no-image.png";
  }

  public function _uploadRelatedImage($data)
  {
    $config['upload_path']          = FCPATH . '/images/gallery/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp|svg';
    $config['encrypt_name']         = true;
    $config['overwrite']            = true;
    $config['max_size']             = 2048 * 1000000;

    $this->load->library('upload', $config);

    $number_of_image = 0;
    if (!empty($_FILES['gallery_image']['name'])) {
      $image = $_FILES['gallery_image'];
      $number_of_image = sizeof($image['name']);
    }

    for ($i = 0; $i < $number_of_image; $i++) {
      if ($image['error'][$i] == 0) {
        $_FILES['image']['name'] = $image['name'][$i];
        $_FILES['image']['type'] = $image['type'][$i];
        $_FILES['image']['tmp_name'] = $image['tmp_name'][$i];
        $_FILES['image']['error'] = $image['error'][$i];
        $_FILES['image']['size'] = $image['size'][$i];

        $this->upload->initialize($config);
        // we retrieve the number of files that were uploaded
        if ($this->upload->do_upload('image')) {
          //insert into table
          $d = array(
            'gallery_image_id' => $this->uuid->v4(),
            'gallery_id' => $data['gallery_id'],
            'gallery_image_name' => $data['gallery_image_name'][$i],
            'gallery_image_file' => $this->upload->data("file_name"),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('user_fullname')
          );
          $this->db->insert('gallery_image', $d);
        } else {
          var_dump($this->upload->display_errors());
          die();
        }
      }
    }
  }

  public function delete_gallery_image($gallery_id, $gallery_image_id)
  {
    $this->m_gallery_video->gallery_image_delete($gallery_image_id);
    redirect(site_url() . '/' . $this->menu['controller'] . '/form/' . $gallery_id);
  }
}
