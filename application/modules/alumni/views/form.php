<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-0">
        <div class="col-sm-6">
          <h5 class="m-0 text-dark"><i class="<?= @$menu['icon'] ?>"></i> <?= @$menu['menu_name'] ?></h5>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Akademik</li>
            <li class="breadcrumb-item active"><?= @$menu['menu_name'] ?></li>
            <li class="breadcrumb-item active"><?= ($id == null) ? 'Tambah' : 'Ubah'; ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Form <?= $menu['menu_name'] ?></h3>
            </div>
            <form id="form" action="<?= site_url() . '/' . $menu['controller'] . '/save/' . $id ?>" method="post" autocomplete="off">
              <div class="card-body">
                <div class="flash-error" data-flasherror="<?= $this->session->flashdata('flash_error') ?>"></div>
                <input type="hidden" class="form-control form-control-sm" name="alumni_id" id="alumni_id" value="<?= @$main['alumni_id'] ?>" required>
                <?php if ($id != null) : ?>
                  <input type="hidden" class="form-control form-control-sm" name="old" id="old" value="<?= @$main['alumni_id'] ?>" required>
                <?php endif; ?>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">Nama Lengkap <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control form-control-sm text-uppercase" name="nama_lengkap" id="nama_lengkap" value="<?= @$main['nama_lengkap'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">Tahun Lulus <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <select class="form-control form-control-sm select2" name="tahun_lulus" id="tahun_lulus">
                      <?php for ($i = 2000; $i < date('Y'); $i++) : ?>
                        <option value="<?= $i ?>" <?= (@$main['tahun_lulus'] == $i) ? 'selected' : '' ?>><?= $i ?></option>
                      <?php endfor; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">Nomor Induk Siswa</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm" name="nis" id="nis" value="<?= @$main['nis'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">NISN <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm" name="nisn" id="nisn" value="<?= @$main['nisn'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">Nomor Ijazah <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm" name="nomor_ijazah" id="nomor_ijazah" value="<?= @$main['nomor_ijazah'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">Jenis Kelamin <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <select class="form-control form-control-sm select2" name="jenis_kelamin" id="jenis_kelamin">
                      <option value="L" <?= @($main['jenis_kelamin'] == 'L') ? 'selected' : '' ?>>Laki-laki</option>
                      <option value="P" <?= @($main['jenis_kelamin'] == 'P') ? 'selected' : '' ?>>Perempuan</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">TTL <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm" name="tmp_lahir" id="tmp_lahir" value="<?= @$main['tmp_lahir'] ?>" required>
                  </div>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm datepicker" name="tgl_lahir" id="tgl_lahir" value="<?= @reverse_date($main['tgl_lahir']) ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">NIK</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm" name="nik" id="nik" value="<?= @$main['nik'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">Email <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="email" id="email" value="<?= @$main['email'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">Telepon <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm" name="telepon" id="telepon" value="<?= @$main['telepon'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">Alamat</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-sm" name="alamat" id="alamat" value="<?= @$main['alamat'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label text-right">Melanjutkan Ke <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <select class="form-control form-control-sm select2" name="lanjut_id" id="lanjut_id">
                      <option value="01" <?= @($main['lanjut_id'] == '01') ? 'selected' : '' ?>>Menganggur</option>
                      <option value="02" <?= @($main['lanjut_id'] == '02') ? 'selected' : '' ?>>Lanjut Studi</option>
                      <option value="03" <?= @($main['lanjut_id'] == '03') ? 'selected' : '' ?>>Bekerja Pemerintahan</option>
                      <option value="04" <?= @($main['lanjut_id'] == '04') ? 'selected' : '' ?>>Bekerja Swasta</option>
                      <option value="05" <?= @($main['lanjut_id'] == '05') ? 'selected' : '' ?>>Wirausaha</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="url" class="col-sm-2 col-form-label text-right">Aktif</label>
                  <div class="col-sm-3">
                    <div class="pretty p-icon mt-2">
                      <input class="icheckbox" type="checkbox" name="is_active" id="is_active" value="1" <?php if (@$main) {
                                                                                                            echo (@$main['is_active']) ? 'checked' : '';
                                                                                                          } else {
                                                                                                            echo 'checked';
                                                                                                          } ?>>
                      <div class="state">
                        <i class="icon fas fa-check"></i><label></label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-10 offset-md-2">
                    <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                    <a class="btn btn-sm btn-default btn-cancel" href="<?= site_url() . '/' . $menu['controller'] . '/' . $menu['url'] ?>"><i class="fas fa-times"></i> Batal</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {
        role_name: {
          remote: {
            type: 'post',
            url: "<?= site_url() . '/' . $menu['controller'] . '/ajax/check_id/' . $id ?>",
            data: {
              'role_name': function() {
                return $('#role_name').val();
              }
            },
            dataType: 'json'
          }
        }
      },
      messages: {
        role_name: {
          remote: "Nama sudah digunakan"
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })
</script>